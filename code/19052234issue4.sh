#!/bin/bash

jdkTargz="$(pwd)/jdk-8u291-linux-x64.tar.gz"
# 检查原先是否已配置java环境变量
jdk1=""
jdk2=""
jdk3=""
flag1=false
flag2=false
flag3=false
checkExist(){
	t1=""
	t2=""
	t3=""
	jdk1=$(grep -n "export JAVA_HOME=.*" /etc/profile | cut -f1 -d':')
	jdk2=$(grep -n "export CLASSPATH=.*\$JAVA_HOME.*" /etc/profile | cut -f1 -d':')
	jdk3=$(grep -n "export PATH=.*\$JAVA_HOME.*" /etc/profile | cut -f1 -d':')
	if [ -n "$jdk1" ] ;then
		echo -n "JAVA_HOME已配置,是否重新配置"
		read -p "(y/n) " t1
		if [ t1 == 'y' ]||[ t1 == 'Y' ];then
			flag1=true
		elif [ t1 == 'n' ]||[ t1 == 'N' ];then
			sed -i "${jdk1}d" /etc/profile
		fi
	fi
	if [ -n "$jdk2" ] ;then
		echo -n "CLASSPATH路径已配置,是否重新配置"
		read -p "(y/n) " t2
		if [ t2 == 'y' ]||[ t2 == 'Y' ];then
			flag2=true
		elif [ t2 == 'n' ]||[ t2 == 'N' ];then
			sed -i "${jdk2}d" /etc/profile
		fi
	fi
	if [ -n "$jdk3" ] ;then
		echo -n "PATH-JAVA路径已配置,是否重新配置"
		read -p "(y/n) " t3
		if [ t3 == 'y' ]||[ t3 == 'Y' ];then
			flag3=true
		elif [ t3 == 'n' ]||[ t3 == 'N' ];then
			sed -i "${jdk3}d" /etc/profile
		fi
	fi
}

# 查询是否有jdk.tar.gz


if [ ! -e $jdkTargz ];then
	wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" "https://enos.itcollege.ee/~jpoial/allalaadimised/jdk8/jdk-8u291-linux-x64.tar.gz"
	jdkTargz="$(pwd)/jdk-8u291-linux-x64.tar.gz"
	echo "— — 下载jdk压缩包完成 — —"
fi
echo "— — 存在jdk压缩包 — —"
echo "正在解压jdk压缩包..."
tar -zxvf $jdkTargz -C /opt
if [ -e "/opt/install/java" ];then
	echo "存在该文件夹，删除..."
	rm -rf /opt/install/java
fi
echo "---------------------------------"
echo "正在建立jdk文件路径..."
echo "---------------------------------"
mkdir -p /opt/install/java/
mv /opt/jdk1.8.0_291 /opt/install/java/java16
#检查配置信息
checkExist
if [ flag1 ]||[ flag2 ]||[ flag3 ];then
	if [ flag1 ];then
		sed -i '$a export JAVA_HOME=/opt/install/java/java16' /etc/profile
	fi
	if [ flag2 ];then
		sed -i '$a export 	CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JAVA_HOME/lib' /etc/profile
	fi
	if [ flag3 ];then
		sed -i '$a export PATH=$PATH:$JAVA_HOME/bin' /etc/profile
	fi
	echo "---------------------------------"
	echo "正在配置jdk环境..."
	echo "---------------------------------"
	echo "JAVA环境配置已完成..."
	echo "---------------------------------"
	echo "正在重新加载配置文件..."
fi
echo "---------------------------------"
source /etc/profile
echo "配置版本信息如下："
java -version
if [ -e "./HelloWorld.java" ];then
	javac HelloWorld.java
	java HelloWorld
fi

source /etc/profile