#!/bin/bash
i=0
bar=""
while [ $i -le 100 ];
do
    printf "[%-100s] %d %c\r" "$bar" "$i" "%"
    let i=i+1
    sleep 0.1
    bar=${bar//">"/"="}
    bar+='>'
done
echo ""